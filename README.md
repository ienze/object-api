### ObjectApi
ObjectApi je ORM (Object Relational Mapper) schopny mapovat RESTful API na PHP objekty pre framework [nette](https://nette.org/). Na spracovanie dotazov je vyuzity [Pest](https://github.com/educoder/pest).

Informácie o inštalácii a použití nájdete v [dokumentácii](docs/sk/index.md)