<?php

namespace Ienze\ObjectApi;

use Nette\Object;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class RepositoryRegistry extends Object {

	/** @var array */
	private $repositories = [];

	/** @var boolean */
	private $useCache;

	public function registerRepository($class, $repository) {
		$this->repositories[$class] = $repository;
	}

	/**
	 * Gets repository for given entity class
	 * 
	 * @param string $class
	 * @return EntityRepository
	 */
	public function getRepository($class) {
		if (isset($this->repositories[$class])) {
			return $this->repositories[$class];
		}
	}

	public function getUseCache() {
		return $this->useCache;
	}

	public function setUseCache($useCache) {
		$this->useCache = $useCache;
	}

}
