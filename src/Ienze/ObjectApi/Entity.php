<?php

namespace Ienze\ObjectApi;

use Nette\Object;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class Entity extends Object {

	public function __toString() {
		$stirng = "Entity " . get_class() . " {\n";
		foreach (get_object_vars($this) as $prop => $val) {
			$stirng .= "  " . $prop . ":" . $val . "\n";
		}
		$stirng .= "}\n";
		return $stirng;
	}

}
