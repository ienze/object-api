<?php

namespace Ienze\ObjectApi;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
interface IDataAccess {

    /**
	 * Find all entities
	 * 
	 * @param EntityRepository $repository
	 * @param array $options
	 * @param string $under Path to use data from
	 * @return array
	 */
    public function findAll(EntityRepository $repository, $options, $under = null);
    
    /**
	 * Find single entity
	 * 
	 * @param EntityRepository $repository
	 * @param string $id
	 * @param array $options
	 * @param string $under Path to use data from
	 * @return object Entity
	 */
    public function find(EntityRepository $repository, $id, $options, $under = null);
    
    /**
	 * Create entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 * @param string $under Path to use data from
	 * @return object Entity
	 */
    public function create(EntityRepository $repository, $entity, $under = null);
	
	/**
	 * Save entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 */
    public function save(EntityRepository $repository, $entity);
	
	/**
	 * Delete entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 */
    public function delete(EntityRepository $repository, $entity);
}
