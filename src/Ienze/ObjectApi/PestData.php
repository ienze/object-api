<?php

namespace Ienze\ObjectApi;

use Ienze\ObjectApi\Bar\DataBar,
	Pest_Exception,
	PestJSON;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class PestData extends PestJSON {

	public function processBody($body) {
		return $this->jsonDecode($body, false);
	}

	protected function checkLastResponseForError() {
		try {
			parent::checkLastResponseForError();
		} catch (Pest_Exception $e) {
			$e->last_response = $this->last_response;
			throw $e;
		}
	}

	public function get($url, $data = array(), $headers = array()) {
		$body = parent::get($url, $data, $headers);
		DataBar::logRequest('GET', $this->last_response);
		return $body;
	}

	public function post($url, $data, $headers = array()) {
		$body = parent::post($url, $data, $headers);
		DataBar::logRequest('POST', $this->last_response);
		return $body;
	}

	public function put($url, $data, $headers = array()) {
		$body = parent::put($url, $data, $headers);
		DataBar::logRequest('PUT', $this->last_response);
		return $body;
	}

	public function delete($url, $headers = array()) {
		$body = parent::delete($url, $headers);
		DataBar::logRequest('DELETE', $this->last_response);
		return $body;
	}

	public function patch($url, $data, $headers = array()) {
		$body = parent::patch($url, $data, $headers);
		DataBar::logRequest('PATCH', $this->last_response);
		return $body;
	}

}
