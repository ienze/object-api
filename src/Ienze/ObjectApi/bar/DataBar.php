<?php

namespace Ienze\ObjectApi\Bar;

use Ienze\ObjectApi\DataManager,
	Nette\DI\Container,
	Nette\Object,
	Tracy\Debugger,
	Tracy\Dumper,
	Tracy\IBarPanel;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class DataBar implements IBarPanel {

	public static $requests = [];

	public static function logRequest($method, $response) {
		if (\Tracy\Debugger::isEnabled()) {
			self::$requests[] = [
				'method' => $method,
				'address' => $response['meta']['url'],
				'code' => $response['meta']['http_code'],
				'body' => $response['body']
			];
		}
	}

	/**
	 * Renders tab.
	 * @return string
	 */
	public function getTab() {
		ob_start();
		$requests = self::$requests;
		require __DIR__ . '/templates/DataBar.tab.phtml';
		return ob_get_clean();
	}

	/**
	 * Renders panel.
	 * @return string
	 */
	public function getPanel() {
		ob_start();
		$requests = self::$requests;
		require __DIR__ . '/templates/DataBar.panel.phtml';
		return ob_get_clean();
	}

}
