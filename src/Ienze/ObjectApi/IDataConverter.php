<?php

namespace Ienze\ObjectApi;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
interface IDataConverter {

	/**
	 * Tells score for given type
	 * 
	 * @param string $type
	 * @return int Score
	 */
	public function scoreForType($type);

	/**
	 * 
	 * @param mixed $value
	 * @param string $type
	 * @return mixed Converted value
	 */
	public function toFrontend($value, $type);

	/**
	 * 
	 * @param mixed $value
	 * @param string $type
	 * @return mixed Converted value
	 */
	public function fromFrontend($value, $type);

	/**
	 * 
	 * @param mixed $value
	 * @param string $type
	 * @return mixed Converted value
	 */
	public function toBackend($value, $type);

	/**
	 * 
	 * @param mixed $value
	 * @param string $type
	 * @return mixed Converted value
	 */
	public function fromBackend($value, $type);
}
