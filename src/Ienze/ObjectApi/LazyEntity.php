<?php

namespace Ienze\ObjectApi;

use Nette\MemberAccessException,
	Nette\Utils\ObjectMixin;

class LazyEntity extends Entity {
	
	/** @var Entity */
	private $entity;
	
	/** @var EntityRepository */
	private $repository;
	
	/** @var string */
	private $id;
	
	/**
	 * Create new lazy loaded entity
	 * 
	 * @param EntityRepository $repository
	 * @param string $id
	 */
	public function __construct(EntityRepository $repository, $id) {
		$this->repository = $repository;
		$this->id = $id;
	}
	
	public function getLazyEntity() {
		if(!$this->entity) {
			$this->entity = $this->repository->getCachedEntity($this->id);
		}
		return $this->entity;
	}

	/**
	 * Call to undefined method.
	 * @param  string  method name
	 * @param  array   arguments
	 * @return mixed
	 * @throws MemberAccessException
	 */
	public function __call($name, $args)
	{
		return ObjectMixin::call($this->getLazyEntity(), $name, $args);
	}


	/**
	 * Returns property value. Do not call directly.
	 * @param  string  property name
	 * @return mixed   property value
	 * @throws MemberAccessException if the property is not defined.
	 */
	public function &__get($name)
	{
		return ObjectMixin::get($this->getLazyEntity(), $name);
	}


	/**
	 * Sets value of a property. Do not call directly.
	 * @param  string  property name
	 * @param  mixed   property value
	 * @return void
	 * @throws MemberAccessException if the property is not defined or is read-only
	 */
	public function __set($name, $value)
	{
		ObjectMixin::set($this->getLazyEntity(), $name, $value);
	}


	/**
	 * Is property defined?
	 * @param  string  property name
	 * @return bool
	 */
	public function __isset($name)
	{
		return ObjectMixin::has($this->getLazyEntity(), $name);
	}


	/**
	 * Access to undeclared property.
	 * @param  string  property name
	 * @return void
	 * @throws MemberAccessException
	 */
	public function __unset($name)
	{
		ObjectMixin::remove($this->getLazyEntity(), $name);
	}
	
}
