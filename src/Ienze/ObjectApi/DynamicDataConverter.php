<?php

namespace Ienze\ObjectApi;

use Nette\DI\Container,
	Nette\DI\PhpReflection,
	Nette\Object;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class DynamicDataConverter extends Object {

	/** @var Container */
	private $container;

	/** @var array */
	private $typeCache = [];

	public function __construct(Container $container) {
		$this->container = $container;
	}

	/**
	 * Converts from middlend(entity) to frontend(strings)
	 * 
	 * @param Entity $entity
	 * @return object
	 */
	public function toFrontend(Entity $entity) {
		$out = [];
		$this->forConverters($entity, function($converter, $propertyName, $propertyType) use ($entity, $out) {
			if ($converter) {
				$converter->toFrontend($entity->$propertyName, $propertyType);
			} else {
				$out[$propertyName] = $entity->$propertyName;
			}
		});
		return (object) $out;
	}

	/**
	 * Converts from frontend(strings) to middlend(entity)
	 * 
	 * @param Entity $entity
	 * @param object|array $data
	 */
	public function fromFrontend(Entity $entity, $data) {
		if (is_array($data)) {
			$data = (object) $data;
		}

		$this->forConverters($entity, function($converter, $propertyName, $propertyType) use ($entity, $data) {
			if ($converter) {
				$entity->$propertyName = $converter->fromFrontend($data->$propertyName, $propertyType);
			} else {
				$entity->$propertyName = $data->$propertyName;
			}
		});
	}

	/**
	 * Converts from middlend(entity) to backend(JSON-compatible)
	 * 
	 * @param Entity $entity
	 * @return mixed
	 */
	public function toBackend(Entity $entity) {
		$out = [];
		$this->forConverters($entity, function($converter, $propertyName, $propertyType) use ($entity, $out) {
			if ($converter) {
				$converter->toBackend($entity->$propertyName, $propertyType);
			} else {
				$out[$propertyName] = $entity->$propertyName;
			}
		});
		return (object) $out;
	}

	/**
	 * Converts from backend(JSON-compatible) to middlend(entity)
	 * 
	 * @param Entity $entity
	 * @param object|array $data
	 */
	public function fromBackend(Entity $entity, $data) {
		if (is_array($data)) {
			$data = (object) $data;
		}

		$this->forConverters($entity, function($converter, $propertyName, $propertyType) use ($entity, $data) {
			if ($converter) {
				$entity->$propertyName = $converter->fromBackend($data->$propertyName, $propertyType);
			} else {
				$entity->$propertyName = $data->$propertyName;
			}
		});
	}

	/**
	 * @param Entity $entity
	 * @param callable $callback
	 */
	private function forConverters(Entity $entity, $callback) {

		$entityReflection = $entity->getReflection();

		foreach ($entityReflection->getProperties() as $property) {

			$var = $property->getAnnotation("var");
			$propertyName = $property->getName();
			
			if ($var) {
				$type = PhpReflection::expandClassName($var, $entityReflection);

				$converter = $this->getConverter($type);

				call_user_func_array($callback, [$converter, $propertyName, $type]);
			} else {
				call_user_func_array($callback, [null, $propertyName, null]);
			}
		}
	}

	/**
	 * Get best converter for given type
	 * 
	 * @param string $type
	 * @return IDataConverter
	 */
	private function getConverter($type) {
		if (!array_key_exists($type, $this->typeCache)) {

			$bestScore = 0;
			$bestConverter = null;
			foreach ($this->container->findByType(IDataConverter::class) as $converterName) {
				$converter = $this->container->getService($converterName);
				$score = $converter->scoreForType($type);
				if ($score > $bestScore) {
					$bestScore = $score;
					$bestConverter = $converter;
				}
			}

			$this->typeCache[$type] = $bestConverter;
		}

		return $this->typeCache[$type];
	}

}
