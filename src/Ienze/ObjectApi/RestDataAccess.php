<?php

namespace Ienze\ObjectApi;

use Nette\InvalidArgumentException,
	Pest_Json_Decode,
	PestJSON;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class RestDataAccess implements IDataAccess {

	/** @var PestJSON */
	private $pest;

	/** @var DynamicDataConverter */
	private $dynamicDataConverter;

	public function __construct(DynamicDataConverter $dynamicDataConverter) {
		$this->dynamicDataConverter = $dynamicDataConverter;
	}

	/**
	 * Setup new RestDataAccess
	 * 
	 * @param string $host
	 */
	public function setup($host) {
		$this->pest = new PestData($host);
	}

	/**
	 * Find all entities
	 * 
	 * @param EntityRepository $repository
	 * @param array $options
	 * @param string $under Path to use data from
	 * @return array
	 */
	public function findAll(EntityRepository $repository, $options, $under = null) {
		$response = $this->pest->get($repository->getName(), $options);

		if ($under) {
			$response = $this->under($response, $under);
		}

		if (is_array($response)) {
			return array_map(function($entity) use ($repository) {
				return $this->parseEntity($repository, $entity, $this->getCachedEntity($repository, $entity));
			}, $response);
		} else {
			$e = new InvalidArgumentException("REST server have to return array!");
			$e->meta = $this->pest->last_response['meta'];
			$e->body = $this->pest->last_response['body'];
			throw $e;
		}
	}

	/**
	 * Find single entity
	 * 
	 * @param EntityRepository $repository
	 * @param string $id
	 * @param array $options
	 * @param string $under Path to use data from
	 * @return object Entity
	 */
	public function find(EntityRepository $repository, $id, $options, $under = null) {
		$response = $this->pest->get($repository->getName() . "/" . $id, $options);

		if ($under) {
			$response = $this->under($response, $under);
		}

		return $this->parseEntity($repository, $response, $this->getCachedEntity($repository, $response));
	}

	protected function parseEntity(EntityRepository $repository, $entity, $newEntity = null) {
		if (!$entity) {
			return null;
		}

		if (is_array($entity)) {
			$entity = (object) $entity;
		}

		if (!$newEntity) {
			$c = $repository->getEntityClass();
			$newEntity = new $c($repository);
		}

		$this->dynamicDataConverter->fromBackend($newEntity, $entity);

		return $newEntity;
	}

	/**
	 * Create entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 * @param string $under Path to use data from
	 * @return object Entity
	 */
	public function create(EntityRepository $repository, $entity, $under = null) {
		$response = $this->pest->post($repository->getName(), $entity);

		if ($under) {
			$response = $this->under($response, $under);
		}

		return $this->parseEntity($repository, $response, $this->getCachedEntity($repository, $entity));
	}

	/**
	 * Save entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 */
	public function save(EntityRepository $repository, $entity) {
		if (is_array($entity)) {
			$entity = (object) $entity;
		}

		if (isset($entity->id) && $entity instanceof Entity) {
			try {
				$response = $this->pest->put($repository->getName() . "/" . $entity->id, $entity);
				$this->parseEntity($repository, $response, $entity);
			} catch (Pest_Json_Decode $e) {
				
			}
		} else {
			throw new InvalidArgumentException("Entity doesn't contains id.");
		}
	}

	/**
	 * Delete entity
	 * 
	 * @param EntityRepository $repository
	 * @param array|object $entity
	 */
	public function delete(EntityRepository $repository, $entity) {
		if (is_array($entity)) {
			$entity = (object) $entity;
		}

		if (isset($entity->id)) {
			$this->pest->delete($repository->getName() . "/" . $entity->id);
		} else {
			throw new InvalidArgumentException("Entity doesn't contains id.");
		}
	}

	/**
	 * Selects data from specified path
	 * 
	 * @param object $response
	 * @param string $under path
	 */
	private function under($response, $under) {
		$underParts = explode('.', $under);

		while ($underParts) {
			$part = array_shift($underParts);
			if (is_object($response)) {
				$response = $response->$part;
			} else {
				throw new InvalidArgumentException("Response path have to match object.");
			}
		}

		return $response;
	}

	private function getCachedEntity($repository, $entity) {
		$idField = $repository->getIdField();

		if (isset($entity->$idField)) {
			return $repository->getCachedEntity($entity->$idField);
		}
	}

}
