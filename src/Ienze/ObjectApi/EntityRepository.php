<?php

namespace Ienze\ObjectApi;

use Nette\InvalidArgumentException,
	Nette\Object;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class EntityRepository extends Object {

	/** @var stirng */
	private $name;

	/** @var stirng */
	private $entityClass;

	/** @var IDataAccess */
	protected $dataAccess;

	/** @var string */
	protected $idField = 'id';

	/** cache */
	private $cacheEntities = [];
	protected $useCache = true;

	public function __construct(IDataAccess $dataAccess, RepositoryRegistry $repositoryRegistry, $name, $entityClass) {
		$this->name = $name;
		$this->entityClass = $entityClass;

		$this->dataAccess = $dataAccess;
		$repositoryRegistry->registerRepository($this->entityClass, $this);
		$this->useCache = $repositoryRegistry->getUseCache();
	}

	/**
	 * Get name of repository
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get class used for entities
	 * 
	 * @return string
	 */
	public function getEntityClass() {
		return $this->entityClass;
	}

	/**
	 * Find all entities
	 * 
	 * @param array $options
	 * @return array
	 */
	public function findAll($options = null) {

		$es = $this->findAllRequest($options);

		if (is_null($es)) {
			throw new InvalidArgumentException("Response cannot be null");
		}
		if (!is_array($es)) {
			throw new InvalidArgumentException("Response has to be array");
		}

		if ($this->useCache) {
			$idField = $this->getIdField();
			foreach ($es as $e) {
				if (isset($e->$idField)) {
					$this->cacheEntities[$e->$idField] = $e;
				}
			}
		}

		return $es;
	}

	protected function findAllRequest($options = null) {
		return $this->dataAccess->findAll($this, $options);
	}

	/**
	 * Find single entity
	 * 
	 * @param string $id
	 * @param array $options
	 * @return object Entity
	 */
	public function find($id, $options = null) {
		
		if ($this->useCache && isset($this->cacheEntities[$id])) {
			return $this->cacheEntities[$id];
		}
		
		$e = $this->findRequest($id, $options);

		if ($this->useCache) {
			$idField = $this->getIdField();
			if (isset($e->$idField)) {
				$this->cacheEntities[$e->$idField] = $e;
			}
		}

		return $e;
	}

	protected function findRequest($id, $options) {
		return $this->dataAccess->find($this, $id, $options);
	}

	/**
	 * Create entity
	 * 
	 * @param array|object $entity
	 * @return object Entity
	 */
	public function create($entity) {
		$e = $this->dataAccess->create($this, $entity);

		if ($this->useCache) {
			$idField = $this->getIdField();
			if (isset($entity->$idField)) {
				$this->cacheEntities[$e->$idField] = $e;
			}
		}

		return $e;
	}

	/**
	 * Save entity
	 * 
	 * @param array|object $entity
	 */
	public function save($entity) {
		return $this->dataAccess->save($this, $entity);
	}

	/**
	 * Delete entity
	 * 
	 * @param array|object $entity
	 */
	public function delete($entity) {

		if ($this->useCache) {
			$idField = $this->getIdField();
			if (isset($entity->$idField) && isset($this->cache[$entity->id])) {
				unset($this->cache[$entity->$idField]);
			}
		}

		return $this->dataAccess->delete($this, $entity);
	}

	public function getIdField() {
		return $this->idField;
	}

}
