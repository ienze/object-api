<?php

namespace Ienze\ObjectApi\DataConverter;

use Ienze\ObjectApi\IDataConverter,
	Nette\Utils\DateTime,
	Nette\Utils\Strings;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class DateDataConverter implements IDataConverter {

	public function scoreForType($type) {
		if (Strings::compare($type, 'date') || Strings::compare($type, 'DateTime') || Strings::compare($type, 'Nette\Utils\DateTime')) {
			return 1;
		}
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function toFrontend($value, $type) {
		return $value->format('Y-m-d H:i:s');
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function fromFrontend($value, $type) {
		return new DateTime($value);
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function toBackend($value, $type) {
		return date("c", $value);
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function fromBackend($value, $type) {
		return new DateTime($value);
	}

}
