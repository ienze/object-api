<?php

namespace Ienze\ObjectApi\DataConverter;

use Ienze\ObjectApi\IDataConverter;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
abstract class ClassDataConverter implements IDataConverter {

	public function scoreForType($type) {

		if ($type == $this->getConvertedClass()) {
			return 100;
		}

		$parents = @class_parents($type);

		if (is_array($parents)) {
			$pos = array_search($this->getConvertedClass(), array_keys($parents));
			if ($pos !== false) {
				return 99 - $pos;
			}
		}

		return 0;
	}

	/**
	 * @return string Name of class converting by this converter
	 */
	protected abstract function getConvertedClass();
}
