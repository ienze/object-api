<?php

namespace Ienze\ObjectApi\DataConverter;

use Ienze\ObjectApi\DynamicDataConverter,
	Ienze\ObjectApi\Entity,
	Ienze\ObjectApi\RepositoryRegistry;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class EntityDataConverter extends ClassDataConverter {

	/** @var DynamicDataConverter */
	private $dynamicDataConverter;

	/** @var RepositoryRegistry */
	private $repositoryRegistry;

	/** @var boolean */
	private $lazy;

	public function __construct(DynamicDataConverter $dynamicDataConverter, RepositoryRegistry $repositoryRegistry) {
		$this->dynamicDataConverter = $dynamicDataConverter;
		$this->repositoryRegistry = $repositoryRegistry;
	}

	protected function getConvertedClass() {
		return Entity::class;
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function toFrontend($value, $type) {
		return $value;
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function fromFrontend($value, $type) {
		return $value;
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function toBackend($value, $type) {
		return $value;
	}

	/**
	 * 
	 * @param mixed $value
	 * @return mixed Converted value
	 */
	public function fromBackend($value, $type) {

		$repo = $this->repositoryRegistry->getRepository($type);

		if ($repo) {

			$idField = $repo->getIdField();

			if (isset($value->$idField)) {

				if ($this->lazy) {
					$entity = new \Ienze\ObjectApi\LazyEntity($repo, $value->$idField);
				} else {
					$entity = $repo->find($value->$idField);
				}
			}

			if (!$entity) {
				$c = $repo->getEntityClass();
				$entity = new $c($repo);
			}
		} else {
			$entity = new $type();
		}

		$this->dynamicDataConverter->fromBackend($entity, $value);

		return $entity;
	}

	function getLazy() {
		return $this->lazy;
	}

	function setLazy($lazy) {
		$this->lazy = $lazy;
	}

}
