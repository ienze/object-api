<?php

namespace Ienze\ObjectApi;

use Ienze\ObjectApi\Bar\DataBar,
	Ienze\ObjectApi\DataConverter\DateDataConverter,
	Ienze\ObjectApi\DataConverter\EntityDataConverter,
	Nette\DI\CompilerExtension;

/**
 * @author Dominik Gmiterko <ienze@ienze.me>
 */
class ObjectApiExtension extends CompilerExtension {

	public $defaultConfig = [
		'host' => 'localhost',
		'cache' => true,
		'lazy' => false,
		'bar' => true
	];

	public function loadConfiguration() {
		parent::loadConfiguration();

		$config = $this->getConfig($this->defaultConfig);

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('repositoryRegistry'))
				->setClass(RepositoryRegistry::class)
				->addSetup('setUseCache', array($config['cache']));

		$builder->addDefinition($this->prefix('dynamicDataConverter'))
				->setClass(DynamicDataConverter::class, array('@container'));

		$builder->addDefinition($this->prefix('access'))
				->setClass(RestDataAccess::class, array($this->prefix('@dynamicDataConverter')))
				->addSetup('setup', array($config['host']));

		/*
		 * Bar
		 */
		if ($config['bar'] && $this->isTracyPresent()) {
			$builder->addDefinition($this->prefix('dataBar'))
					->setClass(DataBar::class);
			$builder->getDefinition('tracy.bar')->addSetup('addPanel', [$builder->getDefinition($this->prefix('dataBar'))]);
		}
		/*
		 * Data converters
		 */
		$builder->addDefinition($this->prefix('entityDataConverter'))
				->setClass(EntityDataConverter::class)
				->addSetup('setLazy', array($config['lazy']));;
		$builder->addDefinition($this->prefix('dateDataConverter'))
				->setClass(DateDataConverter::class);
	}

	/**
	 * @return bool
	 */
	private function isTracyPresent() {
		return interface_exists('Tracy\IBarPanel');
	}

}
