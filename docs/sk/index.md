### ObjectApi
ObjectApi je ORM (Object Relational Mapper) schopny mapovat API na PHP objekty pre framework [nette](https://nette.org/). ObjectApi je prisposobeny pre REST architektúru. Využíva HTTP protokol a jeho 4 metódy (GET, POST, PUT, DELETE). Na spracovanie dotazov je využívaná knižnica [Pest](https://github.com/educoder/pest).

### Inštalácia
Najjednoduchšie sa nainštaluje pomocou composera. V opačnom príade je nutné načítať všetky triedy iným spôsobom (napríklad RobotLoader).

```composer require ienze/object-api```

Do nette konfigurácie nette.neon pridáme záznam o rozšírení a požadované nastavenia.

```
extensions:
    objectApi: Ienze\ObjectApi\ObjectApiExtension

objectApi:
    host: https://api.twitch.tv/kraken
```

### Model
Každá načítavaná adresa musí mať pripavenú entitu. Je to trieda v ktorej sú zadefinovaná štruktúra dat v JSON. Každá hodnota v entite má napísaný typ akého je.

Povedzme, že nám nejaký API server poskytuje údaje v tejto štruktúre.

```json
[
  {
    "id": 1,
    "name": "Dominik"
  },
  {
    "id": 1,
    "name": "Linda"
  },
  {
    "id": 1,
    "name": "Urban"
  }
]
```

### Trieda Entity
K údajom chceme zadefinovať triedu ktorá sa bude používať pre každý záznam tohto typu. Trieda môže vyzerať takto:

```php
<?php

namespace App\Model;

class Person extends \Ienze\ObjectApi\Entity {
    
    /**
     * @var int
     */
    publlic $id;
    
    /**
     * @var string
     */
    public $name;
    
    public function getNameLenght() {
        return str_len($this->name);
    }
}
```

Do tried je možné pridávať vlastné funkcie na ďalšie spracovanie dát ako napríklad getNameLenght().

### Trieda Repository
K údajom sa pristupuje pomocou repozitárov. Každá entita má mať vlastný repozitár.

```php
<?php

namespace App\Model;

class PersonRepository extends \App\Data\EntityRepository {
    public function __construct(\Nette\DI\Container $container $container) {
        parent::__construct($container, 'persons', Person::class); // << použité meno a trieda
    }
}
```

Prvý z pridaných parametrov, 'persons', je názov repozitára. Používa sa na načítavanie údajov. Tvorí vlastne časť adresy. Ak je host '127.0.0.1' a nazov repozitára 'persons' tak dáta sa budú načítavať z adresy '127.0.0.1/persons'.

Druhý pridaný parameter, Person::class, je trieda pre ktorú je tento repozitár určený.

Repozitár je nutné pridať do konfiguracie nette (config.neon) aby sa dala využiť [dependency injection](https://doc.nette.org/cs/2.3/dependency-injection).

```
services:
    - App\Model\PersonRepository
```

### Pouzitie
Na mieste kde chceme dáta využívať injectneme repozitár neiktorým z možných spôsobov.

```php
/** @var \App\Model\PersonRepository @inject */
public $personRepository;
```
alebo
```php
/** @var \App\Model\PersonRepository */
private $personRepository;

public function injectRepositories(\App\Model\PersonRepository $personRepository) {
    $this->personRepository = $personRepository;
}
```

#### findAll()
Až máme dostupný repozitár môžeme využiť všetky jeho (úžasné) funkcie. Prvou z funkcii je findAll(), slúži na načítanie všetkych záznamov z API.

```php
public function renderDefault() {
    $this->template->persons = $this->personRepository->findAll();
}
```

V šablone sa vykresli štandardnym sposobom. Ale taktiez sa pri vykreslovani mozu vyuzivat zadefinovane funkcie ako napriklad $person->getNameLenght(). Taktiež je možné ku všetkým premenným pristupovať pomocou ich get a set funkcie ( $person->getName() ).

```
<ul>
{foreach $persons as $person}
    <li><a n:href="person id=>$person->id">{$person->name} ({$person->getNameLenght()})</li>
{/foreach}
</ul>
```

Vykresli zoznam s odkazmi na ich stranky.
```
- Dominik (7)
- Linda (5)
- Urban (5)
```

Parameter v tejto funkcii je predaný do adresy dotazu, napríklad $personRepository->findAll(['follows' => 'Linda']) načíta údaje z adresy 127.0.0.1/persons?follows=Linda.

#### find($id)
Načitať jeden konkrétny záznam môžeme pomocou tejto funkcie. Funkcii je nutné predať id záznamu. Potom vráti záznam alebo null ak sa takýto záznam nenašiel.

Do presentra pridáme funkciu ktorá bude zobrazovať údaje o jednej osobe.

```
public function renderPerson($id) {
    $this->template->person = $this->personRepository->find($id);
}
```

```
{if $person}
    Táto osoba má meno {$person->getName()}
{else}
    Osoba nebola nájdená
{/if}
```

#### Create
Funkcia create slúži na vloženie nového záznamu. Využíva HTTP metódu POST. Funkcia vracia novú entitu.

```php
$linda = $this->personRepository->create([
    "name" => "Linda"
]);
```

#### Save
Funkcia save slúži na zmenu parametrov záznamu. Využíva HTTP metódu PUT.

```php
$linda = $personRepository->find(8);
$linda->name = "Dominik";

$linda->save();
// alebo
$this->personRepository->save($linda);
```

#### Delete
Funkcia delete slúži na zmazanie záznamu. Využíva HTTP metódu DELETE.

```php
$linda->delete();
// alebo
$this->personRepository->delete($linda);
```
### Rozšírenie repozitára
Repozitár je stále možné rozšíriť v jeho triede. Napríklad pridať funkciu ktorá spracuváva nejaké neštandardné dáta. (Ako by ste to vedeli keby som vám to nepovedal? :P )

### Manuálne dotazy
V prípade, že je to potrebné je stále možné obísť systém a volať a spracovávať dotazy ručne. Je to možné načítaním triedy Ienze\ObjectApi\PestData pomocou dependency injection. Všetky jeho funkcie vracaju JSON objekt(y).

```
public function injectRepositories(\Ienze\ObjectApi\PestData $data) {
    $this->data = $data;
}

public function actionGetTop() {
    $response = $this->data->get('top/10');
    ...
}
```

### Cache
Načítané záznamy sa automaticky ukladajú do cache. Pri ďalšom rovankom dotaze sa použijú už načítané záznamy. Cache zotrváva len počas jedneho dotazu na PHP apikáciu. Cache moze byt vypnuta v nastaveniach:

```
objectApi:
    cache: false
```

Taktiež sa dá vypnúť len pre jeden repozitár:

```php
class PersonRepository extends \App\Data\EntityRepository {
    public function __construct(\Nette\DI\Container $container $container) {
        parent::__construct($container, 'persons', Person::class);
        $this->useCache = false;
    }
}
```

### Authentifikácia
Ak REST server vyžaduje autentifikáciu je možné ju pridať k dotazom. Hoci autentifikácia nieje priamo podporovaná, dá sa jednoducho doplniť pomocou eventu ktorý je volaný pri odosielaní každého dotazu.

### Údajove typy
Všetky premenne v entitách by mali mať zadefinovaný typ pomocou anotácie @var. Primitívne typy ako int alebo string môžu byť vynechané. Avšak na zložitejšie je nutné napísať aby sa na hodnoty aplikovali "konvertory". Je niekoľko preddefinovaných konvertorov ako napríklad konvertor pre dátum. Je možné pridávať aj vlastné.

```php
/**
 * @var DateTime
 */
$publishedAt;
```

#### Relácie medzi typmi
```php
/** @var int */
$id;

/** @var string */
$title;

/** @var App\Model\Author */
$album;
```
Entitu je taktiež možné použiť ako typ v @var. V tomto príklade hore budú na načítanie autora použité údaje z backendu. Je možné predať len id alebo aj všetky dáta. Podla id a zadaneho typu sa ďalšie údaje získajú automaticky.

```json
{
  "id": 374,
  "title": "Stressed Out",
  "author": {
    "id": 21,
    "name": "Twenty One Pilots",
    "Members": [
      "Tyler Joseph",
      "Josh Dun"
    ]
  }
}
```
alebo
```json
{
  "id": 374,
  "title": "Stressed Out",
  "author": {
    "id": 21
  }
}
```

V druhom prípade kedy je poskytnuté len id môžu byť údaje načítané okamžite alebo až v momente keď ich budeme potrebovat. (Napríklad keď sa ich budeme snažit čítať pomocou $song->author->name). Toto správanie sa dá nastaviť globálne aj v nastaveniach..

```
objectApi:
    lazy: true
```

#### Pridanie konvertoru

![ObjectApi convertor](objectApiConvertor.png)

Konvertory sú všetký triedy implementujuce rozhranie IDataConverter. Stačí ich len pridať do konfiguracie Nette ako service a automaticky sa načítajú.

#### ClassDataConverter
Pre konvertovanie tried je pripravená pomocná (abstraktná) trieda ClassDataConverter.

```php
use App\Fruit;
use App\Data\DataConverter\ClassDataConverter;

class FruitDataConverter extends ClassDataConverter {

    protected function getConvertedClass() {
        return Fruit::class;
    }
    
    ...
}
```

### Debug bar
Pre jednoduchšie sledovanie dotazov a odpovedí je dostupný tab na Tracy bare.

![ObjectApi debug bar](objectApiBar.png)

Je možne ho vypnúť v nastaveniach.

```
objectApi:
    bar: false
```
