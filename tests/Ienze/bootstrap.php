<?php

use Ienze\ObjectApi\DynamicDataConverter,
	Ienze\ObjectApi\RepositoryRegistry,
	Ienze\ObjectApi\RestDataAccess,
	Nette\Caching\Storages\DevNullStorage,
	Nette\DI\Container,
	Nette\Loaders\RobotLoader,
	Tester\Assert,
	Tester\Environment;

// The Nette Tester command-line runner can be
// invoked through the command: ../vendor/bin/tester .

define('TEST_HOST', '127.0.0.1:19384');

if (@!include __DIR__ . '/../../../vendor/autoload.php') {
	echo 'Install Nette Tester using `composer install`';
	exit(1);
}

$robotLoader = new RobotLoader();
$robotLoader->setCacheStorage(new DevNullStorage());
$robotLoader->addDirectory(__DIR__ . '/../../src/Ienze');
$robotLoader->addDirectory(__DIR__ . '/ObjectApi/model');
$robotLoader->register();

// configure environment
Environment::setup();
date_default_timezone_set('Europe/Prague');


//setup
$container = new Container();

$dynamicDataConverter = new DynamicDataConverter($container);

$restDataAccess = new RestDataAccess($dynamicDataConverter);
$restDataAccess->setup(TEST_HOST);

$repositoryRegistry = new RepositoryRegistry();

$testRepository = new TestRepository($restDataAccess, $repositoryRegistry);

// output buffer level check
register_shutdown_function(function ($level) {
	Assert::same($level, ob_get_level());
}, ob_get_level());

function test(Closure $function) {
	$function();
}

function equalEntity($expected, $actual) {
	if (is_array($expected) != is_array($actual)) {
		Assert::fail("Both expected and actual have to be (or not be) arrays");
	}

	if (is_array($expected)) {

		if (count($expected) != count($actual)) {
			Assert::fail("Both expected and actual have to be same lenght", count($actual), count($expected));
		}

		for ($i = 0; $i < count($expected); $i++) {
			equalEntity($expected[$i], $actual[$i]);
		}
	} else {
		if (get_class($expected) != get_class($actual)) {
			Assert::fail("Both expected and actual have to be same class", get_class($actual), get_class($expected));
		}

		if (get_object_vars($expected) != get_object_vars($actual)) {
			Assert::fail("Both expected and actual have to have same public properties", get_object_vars($actual), get_object_vars($expected));
		}
	}
}

class Notes {

	public static $notes = [];

	public static function add($message) {
		self::$notes[] = $message;
	}

	public static function fetch() {
		$res = self::$notes;
		self::$notes = [];
		return $res;
	}

}
