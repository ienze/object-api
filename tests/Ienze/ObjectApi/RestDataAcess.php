<?php

/**
 * Test: RestDataAccess
 */
require __DIR__ . '/../bootstrap.php';

use Tester\Assert;

//under
//test(function() use ($restDataAccess) {
//	Assert::equal('ok', $restDataAccess->under((object) [
//						'test' => (object) [
//							'path' => 'ok'
//						]
//					], 'test.path'));
//});

//save
test(function() use($restDataAccess, $testRepository) {

	$entity = new Test($testRepository);
	$entity->id = 8;
	$entity->name = 'Another name';

	$restDataAccess->save($testRepository, $entity);

	Assert::equal('Another name', $entity->name);
});


//save error
Assert::exception(function() use($restDataAccess, $testRepository) {
	$restDataAccess->delete($testRepository, [
		'some' => 'random',
		'entity' => 'without id'
	]);
}, \Nette\InvalidArgumentException::class);


//delete error
Assert::exception(function() use($restDataAccess, $testRepository) {
	$restDataAccess->delete($testRepository, [
		'some' => 'random',
		'entity' => 'without id'
	]);
}, \Nette\InvalidArgumentException::class);