<?php

/**
 * Test: RepositoryRegistry
 */
require __DIR__ . '/../bootstrap.php';

use Tester\Assert;

test(function() use($repositoryRegistry, $testRepository) {
	Assert::equal($testRepository, $repositoryRegistry->getRepository(Test::class));
});

test(function() use($repositoryRegistry) {
	Assert::null($repositoryRegistry->getRepository(DateTime::class));
});
