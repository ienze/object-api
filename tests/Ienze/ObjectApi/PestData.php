<?php

/**
 * Test: RestDataAccess
 */
require __DIR__ . '/../bootstrap.php';

use Tester\Assert;

$pest = new Ienze\ObjectApi\PestData(TEST_HOST);

test(function() use($pest) {
	Assert::exception(function() use($pest) {
		$pest->get('some-not-found-address');
	}, Pest_NotFound::class);
});

test(function() use($pest) {
	$expected = json_decode('[{"id":1,"name":"rsenyzujgg"},{"id":2,"name":"hrkruecskz"},{"id":3,"name":"kdljqabcci"},{"id":4,"name":"cksfucgufs"},{"id":8,"name":"Ienze"}]');
	Assert::isEqual($expected, $pest->get('test'));
});