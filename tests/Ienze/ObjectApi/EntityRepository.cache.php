<?php

/**
 * Test: RestDataAccess
 */
require __DIR__ . '/../bootstrap.php';

use Tester\Assert;

test(function() use($testRepository){
	
	$testRepository->useCache = true;
	
	Assert::null($testRepository->getCachedEntity(8));
	
	Assert::notSame(null, $testRepository->find(8));
	
	Assert::notSame(null, $testRepository->getCachedEntity(8));
	
	$testRepository->useCache = false;
	
	Assert::null($testRepository->getCachedEntity(8));
});