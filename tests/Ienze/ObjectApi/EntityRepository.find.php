<?php

/**
 * Test: RestDataAccess
 */
require __DIR__ . '/../bootstrap.php';

use Tester\Assert;


$testUnderRepository = new TestUnderRepository($restDataAccess, $repositoryRegistry, 'excelent.path');

$testData = include __DIR__ . '/server/test/data.php';
$testData8 = $testData[4];

//findAll
test(function() use ($testRepository, $testData) {
	equalEntity($testData, $testRepository->findAll());
});

//find
test(function() use ($testRepository, $testData8) {
	equalEntity($testData8, $testRepository->find(8));
});

//under
test(function() use ($restDataAccess) {
	Assert::equal('ok', $restDataAccess->under((object) [
						'test' => (object) [
							'path' => 'ok'
						]
					], 'test.path'));
});

//findAll under
test(function() use ($testUnderRepository, $testData) {
	equalEntity($testData, $testUnderRepository->findAll());
});

//find under
test(function() use ($testUnderRepository, $testData8) {
	equalEntity($testData8, $testUnderRepository->find(8));
});
