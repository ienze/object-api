<?php

use Ienze\ObjectApi\EntityRepository,
	Ienze\ObjectApi\IDataAccess,
	Ienze\ObjectApi\RepositoryRegistry;

class TestUnderRepository extends EntityRepository {

	private $under;

	public function __construct(IDataAccess $dataAccess, RepositoryRegistry $repositoryRegistry, $under) {
		parent::__construct($dataAccess, $repositoryRegistry, 'test/under', Test::class);
		$this->under = $under;
	}

	protected function findAllRequest($options = null) {
		return $this->dataAccess->findAll($this, $options, $this->under);
	}

	protected function findRequest($id, $options) {
		return $this->dataAccess->find($this, $id, $options, $this->under);
	}

}
