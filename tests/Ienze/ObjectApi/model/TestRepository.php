<?php

use Ienze\ObjectApi\EntityRepository,
	Ienze\ObjectApi\IDataAccess,
	Ienze\ObjectApi\RepositoryRegistry;


class TestRepository extends EntityRepository {

	public function __construct(IDataAccess $dataAccess, RepositoryRegistry $repositoryRegistry) {
		parent::__construct($dataAccess, $repositoryRegistry, 'test', Test::class);
	}

}
