<?php

if ($_SERVER['REQUEST_METHOD'] == 'PUT') {

	echo json_encode($_POST);
	
} elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {

	header('Content-Type: application/json');
	echo json_encode([
		'id' => 8,
		'name' => 'Ienze'
	]);
}