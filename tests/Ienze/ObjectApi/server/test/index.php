<?php

require __DIR__ . '/../bootstrap.php';

$data = include __DIR__ . '/data.php';

header('Content-Type: application/json');
echo json_encode($data);
