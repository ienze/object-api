<?php

if (!class_exists('App\Data\Entity')) {
	if (@!include __DIR__ . '/../../../../../vendor/autoload.php') {
		echo 'Install Nette Tester using `composer install`';
		exit(1);
	}

	$robotLoader = new \Nette\Loaders\RobotLoader();
	$robotLoader->setCacheStorage(new Nette\Caching\Storages\DevNullStorage());
	$robotLoader->addDirectory(__DIR__ . '/../../../../src/Ienze');
	$robotLoader->addDirectory(__DIR__ . '/../model');
	$robotLoader->register();
}